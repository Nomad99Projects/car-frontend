package pl.car.carfrontend.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static pl.car.carfrontend.config.ApplicationConstants.CORRELATION_ID;

@Slf4j
@Component
public class ResponseFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return FilterUtils.POST_FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();

        log.debug("Adding correlation id to outbound headers. {}", FilterUtils.getCorrelationId());
        ctx.getResponse().addHeader(CORRELATION_ID, FilterUtils.getCorrelationId());
        log.debug("Completing outgoing request for {}.", ctx.getRequest().getRequestURI());

        return null;
    }
}
