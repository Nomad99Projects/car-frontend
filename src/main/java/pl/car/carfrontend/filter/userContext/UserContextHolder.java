package pl.car.carfrontend.filter.userContext;

import io.jsonwebtoken.lang.Assert;

import java.util.Objects;

@SuppressWarnings("Duplicates")
public class UserContextHolder {

    private static final ThreadLocal<UserContext> userContext = new ThreadLocal<>();

    public static UserContext getContext() {
        UserContext context = UserContextHolder.userContext.get();
        if (Objects.isNull(context)) {
            context = createEmptyContext();
            userContext.set(context);
        }

        return userContext.get();
    }

    public static void setContext(UserContext context) {
        Assert.notNull(context, "Only non-null UserContext instances are permitted");

        userContext.set(context);
    }

    private static UserContext createEmptyContext() {
        return new UserContext();
    }
}
