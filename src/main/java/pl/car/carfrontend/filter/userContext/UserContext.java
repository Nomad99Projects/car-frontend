package pl.car.carfrontend.filter.userContext;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("Duplicates")
public class UserContext {

    private String correlationId = "";
    private String authToken = "";

}
