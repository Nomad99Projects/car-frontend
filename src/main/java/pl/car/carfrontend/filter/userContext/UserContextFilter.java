package pl.car.carfrontend.filter.userContext;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.car.carfrontend.security.config.JWTProperties;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static pl.car.carfrontend.config.ApplicationConstants.CORRELATION_ID;


@Slf4j
@Component
@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public class UserContextFilter implements Filter {

    private final JWTProperties jwtProperties;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        UserContextHolder.getContext().setCorrelationId(httpServletRequest.getHeader(CORRELATION_ID));
        UserContextHolder.getContext().setAuthToken(httpServletRequest.getHeader(jwtProperties.getHeader()));

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
