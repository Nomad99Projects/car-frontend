package pl.car.carfrontend.filter;

import com.netflix.zuul.context.RequestContext;

import static pl.car.carfrontend.config.ApplicationConstants.CORRELATION_ID;

public class FilterUtils {

    public static final String PRE_FILTER_TYPE = "pre";
    public static final String POST_FILTER_TYPE = "post";

    public static String getCorrelationId() {
        RequestContext ctx = RequestContext.getCurrentContext();

        return ctx.getRequest().getHeader(CORRELATION_ID) != null ? ctx.getRequest().getHeader(CORRELATION_ID) : ctx.getZuulRequestHeaders().get(CORRELATION_ID);
    }

    public static void setCorrelationId(String correlationId){
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader(CORRELATION_ID, correlationId);
    }

    public static String getServiceId(){
        RequestContext ctx = RequestContext.getCurrentContext();

        return ctx.get("serviceId") == null ? "" : ctx.get("serviceId").toString();
    }
}
