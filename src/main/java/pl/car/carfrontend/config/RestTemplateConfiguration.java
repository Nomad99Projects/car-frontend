package pl.car.carfrontend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import pl.car.carfrontend.filter.userContext.UserContextInterceptor;
import pl.car.carfrontend.security.config.JWTProperties;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Configuration
public class RestTemplateConfiguration {

    private JWTProperties jwtProperties;

    @Autowired
    public void setJwtProperties(JWTProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        RestTemplate template = new RestTemplate();
        List<ClientHttpRequestInterceptor> interceptors = template.getInterceptors();
        if (Objects.isNull(interceptors)) {
            template.setInterceptors(Collections.singletonList(new UserContextInterceptor(jwtProperties)));
        } else {
            interceptors.add(new UserContextInterceptor(jwtProperties));
            template.setInterceptors(interceptors);
        }

        return template;
    }
}
